import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native'

export default class Tombol extends Component {
  render() {
    return (
        // <View style={styles.viewStyle}>
        //  <Text>asd</Text>
        <View style={styles.styles}>
        <TouchableOpacity style={styles.tombolKiri}>
            <Text style={styles.textKiri}> Add List Here </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.tombolKanan}>
            <Text style={styles.textKanan}> Submit </Text>
        </TouchableOpacity>
        </View>
        // </View>
    )
  }
}

const styles = StyleSheet.create({
  styles:{
    //   flex: 1,
      flexDirection: "row",
      justifyContent: "space-around",
      margin: 10,
    //   borderRadius: 2,
    //   padding: 10,
    //   alignContent: "flex-end",
    //   alignItems: "flex-end"
  },
  textKanan:{
      fontFamily: "Pacifico-Regular",
      marginTop: 10,
      marginRight:20,
      marginLeft:20,
      marginBottom: 10,
      fontSize:20,
    //   borderRadius: 5,
    // flex:1,
    // flexDirection: "column",
    // justifyContent: "flex-end",
    // alignContent: "flex-end"
  },
  textKiri:{
    fontFamily: "Pacifico-Regular",
      marginBottom:10,
      fontSize: 20,
      marginTop:10,
      marginRight: 60,
      marginLeft: 60,
  },
  tombolKanan:{
      borderWidth: 1,
      borderColor: "black",
    //   padding: 5,
      margin: 5,
      borderRadius: 5,
      elevation:3
  },
  tombolKiri:{
    elevation:3,
    borderWidth: 1,
    borderColor: "black",
  //   padding: 5,
    margin: 5,
    borderRadius: 5,
    },

  
})
