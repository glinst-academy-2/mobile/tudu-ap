import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {Provider} from 'react-redux'
import reducers from './source_code/reducer/index'
import ReduxThunk from 'redux-thunk'
import {createStore,applyMiddleware} from 'redux'
import App from './App'
import firebase from '@firebase/app'
import Navigasi from './Navigasi'

export default class Home extends Component {
    componentWillMount(){
      var config = {
        apiKey: "AIzaSyAWByU43jLeTZE-A5bPDblT62OXQ8wnz28",
        authDomain: "tuduapp-34a39.firebaseapp.com",
        databaseURL: "https://tuduapp-34a39.firebaseio.com",
        projectId: "tuduapp-34a39",
        storageBucket: "tuduapp-34a39.appspot.com",
        messagingSenderId: "837151760104"
      };
      firebase.initializeApp(config);
    }
  render() {
    const store=createStore(reducers,{},applyMiddleware(ReduxThunk))
    return (
        <Provider store={store}>
            {/* <App /> */}
            <Navigasi />
        </Provider>
    )
  }
}
