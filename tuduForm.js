import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {CardSection,Inputt} from './common/index'
import {connect} from 'react-redux'
import {textUpdate} from './source_code/action/dataAction'

 class tuduForm extends Component {
  render() {
    return (
      <View>
            <CardSection>
                {/* <Text> Tulis dibawah:</Text> */}
            <Inputt

                placeholder="Tulis disini TuDu yang ingin anda lakukan"
                onChangeText={value=>this.props.textUpdate({prop:"tudulist",value})}
                value={this.props.tudulist}
            />
            </CardSection>
 
      </View>
    )
  }
}

const mapStateToProps=(state)=>{
    const {tudulist} = state.tudu
    return {tudulist}
}

export default connect(mapStateToProps,{textUpdate})(tuduForm);