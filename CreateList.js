import React, { Component } from 'react'
import { Text, View, } from 'react-native'
import {CardSection,Card,Inputt,Button} from './common/index'
import {textUpdate,tuduCreate} from './source_code/action/dataAction'
import {connect} from 'react-redux'
import TuduForm from './tuduForm'
// import {textUpdate} from './source_code/action/dataAction'

class CreateList extends Component {
    onTextChanged(text){
        this.props.textChanged(text)
    }

    onButtonPress(){
        const {tudulist} = this.props;

        this.props.tuduCreate({tudulist});
    }
  render() {
    return (
        <Card>
            <TuduForm 
            // {...this.props}
            />
            <CardSection>
                <Button onPress={this.onButtonPress.bind(this)}>
                    Create !
                </Button>
            </CardSection>
        </Card>
    )
  }
}

const mapStateToProps=(state)=>{
    const {tudulist} = state.tudu

    return {tudulist};
}

export default connect (mapStateToProps,{textUpdate,tuduCreate})(CreateList);