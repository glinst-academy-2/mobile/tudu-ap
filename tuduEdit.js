import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {connect} from 'react-redux'
import {Card,CardSection,Button,Confirm} from './common/index'
import {textUpdate,tuduSave,tududelete} from './source_code/action/dataAction'
import TuduForm from './tuduForm'
import _ from 'lodash'

class tuduEdit extends Component {

    state={showModal:false}

    componentWillMount(){
        _.each(this.props.tuduu,(value,prop)=>{
            this.props.textUpdate({prop,value});
        })
    }

    onButtonPress(){
        const {tudulist}=this.props;
        this.props.tuduSave({tudulist,uid:this.props.tuduu.uid})
        // console.log(this.props.tuduu);
    }

    onAccept(){
        const {uid}=this.props.tuduu;
        this.props.tududelete({uid})
    }

    onDecline(){
        this.setState({showModal:false})
    }

  render() {
      console.log(this.props.tuduu)
    return (
        <Card>
            <TuduForm />
            <CardSection>
                <Button onPress={this.onButtonPress.bind(this)}>
                    Save Changes! 
                </Button>
            </CardSection>

            <CardSection>
                <Button onPress={()=>this.setState({showModal:!this.state.showModal})}>
                    Delete
                </Button>
            </CardSection>

            <Confirm visible={this.state.showModal}
                onAccept={this.onAccept.bind(this)}
                onDecline={this.onDecline.bind(this)}
            >
                Are u sure?
            </Confirm>
        </Card>
    )
  } 
}

const mapStateToProps =(state)=>{
    const {tudulist} =state.tudu;
    return{tudulist};   

}

export default connect (mapStateToProps, {textUpdate,tuduSave,tududelete}) (tuduEdit);