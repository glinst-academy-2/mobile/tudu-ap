import React, { Component } from 'react'
import { ListView, Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import {Card,CardSection, Header} from './common/index'
// import {connect} from 'react-redux'
import {fetchData} from './source_code/action/dataAction'
import {connect} from 'react-redux'
import _ from 'lodash'
import ListItem from './ListItem'
import {Actions} from 'react-native-router-flux'

class App extends Component {
  componentWillMount(){
    this.props.fetchData();

    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps){

    this.createDataSource(nextProps)
  }

  createDataSource({datas}){
    const ds =new ListView.DataSource({
      rowHasChanged:(r1,r2) => r1 !== r2
    })

    this.dataSource=ds.cloneWithRows(datas)
  }

  renderRow(dataa){
    return <ListItem dataa={dataa} />
  }

  render() {
    // console.log(this.props)
    return (
      <Card>
        <Header headerText={"TuDuApp"}/>
        <CardSection>
          <Text>Test</Text>
        </CardSection>
        <CardSection>  
          <TouchableOpacity
          onPress={Actions.createList}>
            <Text style={styles.buttonStyle}>Create!</Text>
          </TouchableOpacity>
        </CardSection>
        <CardSection>
          <ListView
            enableEmptySections
            dataSource={this.dataSource}
            renderRow={this.renderRow}/>
        </CardSection>
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  buttonStyle: {
    // flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#007aff',
    marginLeft: 5,
    marginRight: 5,
    padding:5,
  }
})

const mapStateToProps=state=>{
  const datas=_.map(state.datas,(val,uid)=>{
    return {...val,uid};
  })

  return {datas};
}

export default connect (mapStateToProps, {fetchData})(App);