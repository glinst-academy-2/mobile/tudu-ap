const INITIAL_STATE={
    tudulist:"",
}

export default (state=INITIAL_STATE,action) =>{
    // console.log(action);
    switch(action.type){
        case "textChanged":
            return {...state,[action.payload.prop]:action.payload.value};
        case "tuduClearCreate":
            return INITIAL_STATE;
        case "isDone":
            return {...INITIAL_STATE,isDone:!state.isDone}
        default:
            return state;
    }
}