import dataReducer from './dataReducer'
import {combineReducers} from 'redux'
import tuduReducer from './tuduReducer'

export default combineReducers({
    datas:dataReducer,
    tudu:tuduReducer,
})