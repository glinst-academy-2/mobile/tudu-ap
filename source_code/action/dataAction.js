import firebase from '@firebase/app'
import '@firebase/database'
import {Actions} from 'react-native-router-flux'

export const fetchData=()=>{
    return (dispatch) =>{
        firebase.database().ref(`list`)
        .on('value',snapshot=>{
            dispatch({type:"fetch_data",payload:snapshot.val()});
        });
    }
}

export const textUpdate=({prop,value})=>{
    return {
        type:"textChanged",
        payload:{prop,value}
    }
}

export const isDone=(state)=>{
    return{
        type:"isDone",
    }
}

export const tuduCreate=({tudulist})=>{
    return (dispatch) =>{
    firebase.database().ref(`list`)
        .push({tudulist})
        .then(()=>{
            dispatch({type:"tuduClearCreate"})
            Actions.pop()
        })
    }
}

export const tuduSave=({tudulist,uid})=>{
    return (dispatch) => {
        firebase.database().ref(`/list/${uid}`)
        .set({tudulist})
        .then(()=>{
            dispatch({type:"tuduClearCreate"})
            console.log("save")
            Actions.pop()
        })
    }
}

export const tududelete=({uid})=>{
    return(dispatch)=>{
        firebase.database().ref(`/list/${uid}`)
        .remove()
        .then(()=>{
            dispatch({type:"tuduClearCreate"})
            Actions.pop()
        })

    }
}

