import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {Scene, Router} from 'react-native-router-flux'
import App from './App'
import CreateList from './CreateList'
import tuduEdit from './tuduEdit'

export default class Navigasi extends Component {
  render() {
    return (
        <Router>
            <Scene key="root">
                <Scene key="menuUtama"
                    component={App}
                    title="TuDuApp"/>
                <Scene key="createList"
                    component={CreateList}
                    title="Form Create List"/>
                <Scene key="tuduEdit" 
                    component={tuduEdit}
                    title="Form Edit List"/>
            </Scene>
        </Router>

    )
  }
}
