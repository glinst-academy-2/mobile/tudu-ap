import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import {CardSection} from './common/index'
import {Actions} from 'react-native-router-flux'  
// import {CheckBox} from 'react-native-elements'

export default class ListItem extends Component {

  onRowPress(){
    Actions.tuduEdit({tuduu:this.props.dataa})
    // Actions.createList();
  }

  render() {
      // console.log(this.props.dataa)
      const name =this.props.dataa.tudulist;
    return (

      <View style={styles.style}>
        <View style={{flex:1}}>
        <TouchableOpacity onPress={this.onRowPress.bind(this)}>
          <CardSection>
              <Text style={{fontSize:18,paddingLeft:15}}>
                  {name}
              </Text>
          </CardSection>
        </TouchableOpacity>
        </View>
        <View style={{flex:1}}>
        {/* <CardSection> */}
          {/* <CheckBox 
            title="Done?"
            checked={this.state.checked}/> */}
        {/* <Text style={{fontSize:18,paddingLeft:15}}>
         Rangga  
         </Text> */}
        {/* </CardSection> */}
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  style:{
    flexDirection:'row',
    flex: 1,
  }
})
